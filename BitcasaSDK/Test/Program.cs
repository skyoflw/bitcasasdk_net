﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BitcasaSDK;
using BitcasaSDK.BParameter;
using BitcasaSDK.BObj;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new BitcasaClient("ba8902128186b046b30f09f270a389c0", "US1_2c154723201e451b87d8395e18ed00e6_acbcda21");

            var l = new BParameter<List<BObject>>();

            l.Callback_Successed = z =>
            {
                Console.WriteLine(z.Count);
            };
            l.Callback_Failed = z =>
            {
                throw new Exception();
            };
            l.Callback_Completed = () =>
            {
                Console.WriteLine("completed");
            };

            client.ListRoot(BitcasaSDK.BRequest.CategoryType.Everything, BitcasaSDK.BRequest.DepthType.Infinite, 0, l);

            Console.Read();
        }
    }
}
