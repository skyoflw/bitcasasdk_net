﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK.BParameter
{
    public class BParameter<TSuccess, TFailed>
    {
        public Action<TSuccess> Callback_Successed;

        public Action<TFailed> Callback_Failed;

        public Action Callback_Completed;

        public Action Callback_Cancelled;

        public ICancelled CancelToken;
    }
}
