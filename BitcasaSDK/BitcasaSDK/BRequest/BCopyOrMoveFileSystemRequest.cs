﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK.BRequest
{
    internal class BCopyOrMoveFileSystemRequest : BOperationFileSystemRequest
    {
        public BCopyOrMoveFileSystemRequest(
            bool isFile,
            string token, string from, string to,
            string filename, bool isMove)
            : base(token, from, filename)
        {
#if DEBUG
            if (to == null)
                throw new ArgumentException(
                    "BCopyOrMoveFolderRequest(): to can not be null.");
#endif
            this.IsFile = IsFile;
            this.To = to;
            this.IsMove = isMove;
        }

        private bool IsFile;

        private bool IsMove;

        private string To;

        protected override System.IO.MemoryStream GenerateBody()
        {
            var ms = base.GenerateBody();

            StringBuilder sb = new StringBuilder();
            sb.Append("&");
            sb.Append("to");
            sb.Append("=");
            sb.Append(this.To);
            var b = sb.ToString().ToUtf8();
            ms.Write(b, 0, b.Length);

            return ms;
        }

        protected override string Operation
        {
            get { return IsMove ? "move" : "copy"; }
        }

        protected override string BaseUrl
        {
            get
            {
                return IsFile ? FILE_URL : FOLDER_URL;
            }
        }
    }
}
