﻿using BitcasaSDK.BJson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK.BRequest
{
    public abstract class BRequest
    {
        /// <summary>
        /// "https://developer.api.bitcasa.com/v1"
        /// </summary>
        public const string BASE_API_URL = "https://developer.api.bitcasa.com/v1";
        public const string AUTHENTUCATION_URL = BASE_API_URL + "/oauth2/authenticate?client_id={0}&redirect={1}";
        public const string GRANT_ACCESS_TOKEN_URL = BASE_API_URL + "/oauth2/access_token";

        /// <summary>
        /// "https://developer.api.bitcasa.com/v1/folders"
        /// </summary>
        protected const string FOLDER_URL = BASE_API_URL + "/folders";
        /// <summary>
        /// "https://developer.api.bitcasa.com/v1/files"
        /// </summary>
        protected const string FILE_URL = BASE_API_URL + "/files";
        /// <summary>
        /// "https://developer.api.bitcasa.com/v1/user"
        /// </summary>
        protected const string USER_URL = BASE_API_URL + "/user";

        public const string FIELD_NAME_SECRET = "secret";
        public const string FIELD_NAME_CODE = "code";

        protected const string CONTENT_TYPE_JSON = "application/json; charset=UTF-8";
        protected const string CONTENT_TYPE_UTF8_PLAIN_TEXT = "text/plain; charset=UTF-8";
        protected const string CONTENT_TYPE_OCTET_STREAM = "application/octet-stream";
        protected const string CONTENT_TYPE_DELTA_STREAM = "application/delta-data";
        protected const string CONTENT_TYPE_URLENCODE = "application/x-www-form-urlencoded";

        protected const string METHOD_POST = "POST";
        protected const string METHOD_GET = "GET";
        protected const string METHOD_DELETE = "DELETE";

        public bool IsCancelled;

        protected HttpWebRequest CurrentRequest;

        protected HttpWebResponse CurrentResponse;

        protected byte[] entity;

        /// <summary>
        /// 构造 URL 参数，包括第一个 ? 号
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected virtual string ConstructURLParameters(Dictionary<string, string> parameters)
        {
            string result;
            if (parameters == null || parameters.Count == 0) return "";
            StringBuilder sb = new StringBuilder();
            sb.Append("?");
            foreach (KeyValuePair<string, string> i in parameters)
            {
                string encodedKey = System.Net.WebUtility.UrlEncode(i.Key);
                //string encodedValue = HttpUtility.UrlEncode(i.Value);
                string encodedValue = i.Value;
                sb.Append(encodedKey)
                            .Append("=")
                            .Append(encodedValue)
                            .Append("&");
            }
            sb.Remove(sb.Length - 1, 1);
            result = sb.ToString();
            return result;
        }

        /// <summary>
        /// 配置将要发出的 json
        /// </summary>
        /// <param name="json"></param>
        protected void ConfigJsonPostRequest(BaseJson obj)
        {
            entity = obj.ToBytes();
            CurrentRequest.Method = METHOD_POST;
            CurrentRequest.ContentType = CONTENT_TYPE_JSON;
            CurrentRequest.ContentLength = entity.Length;
        }
    }
}
