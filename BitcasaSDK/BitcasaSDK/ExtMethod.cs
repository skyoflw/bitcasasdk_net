﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK
{
    public static class ExtMethod
    {
        #region date time

        /// <summary>
        /// linq 数据库最小时间为 1753/1/1-12:0:0
        /// 这里选取文件系统还没诞生的 1900 年
        /// </summary>
        private static readonly DateTime MinDateTime = new DateTime(1900, 1, 1, 0, 0, 0);
        /// <summary>
        /// 获取合法的 linq 数据库时间
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTime LinqTime(this DateTime dt)
        {
            if (dt < MinDateTime)
                return MinDateTime;
            else
                return dt;
        }

        public static DateTime ToLocalTime(long time)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                .AddMilliseconds(time).ToLocalTime();
        }

        #endregion

        public static byte[] ToUtf8(this string str)
        {
            return UTF8Encoding.UTF8.GetBytes(str);
        }
    }
}
