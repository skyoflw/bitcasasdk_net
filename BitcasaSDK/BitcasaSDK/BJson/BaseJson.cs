﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;

namespace BitcasaSDK.BJson
{
    [DataContract]
    public abstract class BaseJson
    {
        [DataMember(Name = "error")]
        public BErrorJson Error;

        /// <summary>
        /// 将此对象序列化
        /// </summary>
        /// <returns></returns>
        public string Serialize()
        {
            return WriteFromObject(this);
        }

        /// <summary>
        /// 将此对象序列化之后再进行 UTF-8 编码
        /// </summary>
        /// <returns></returns>
        public byte[] ToBytes()
        {
            return Encoding.UTF8.GetBytes(this.Serialize());
        }

        #region Json 静态方法

        protected static string WriteFromObject(object obj)
        {
            using (var ms = new MemoryStream())
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(obj.GetType());
                ser.WriteObject(ms, obj);
                byte[] json = ms.ToArray();
                return Encoding.UTF8.GetString(json, 0, json.Length);
            }
        }

        protected static T ReadFromStringToObject<T>(string json)
        {
            return ReadFromStringToObject<T>(Encoding.UTF8.GetBytes(json));
        }

        protected static T ReadFromStringToObject<T>(byte[] json_utf8)
        {
            using (var ms = new MemoryStream(json_utf8))
            {
                var ser = new DataContractJsonSerializer(typeof(T));
                T obj = (T)ser.ReadObject(ms);
                return obj;
            }
        }

        #endregion
    }
}
