﻿using BitcasaSDK.BObj;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK.BJson
{
    [DataContract]
    internal class BListRootJson : BitcasaJson<BListRootJson, BListRootJson.Content>
    {
        [DataContract]
        internal class Content
        {
            [DataMember(Name = "items")]
            public List<BRoot> Items;
        }
    }
}
