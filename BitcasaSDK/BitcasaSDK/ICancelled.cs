﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BitcasaSDK
{
    public interface ICancelled
    {
        event EventHandler Cancelled;
    }
}
