﻿using BitcasaSDK.BObj;
using BitcasaSDK.BParameter;
using BitcasaSDK.BRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK
{
    public class BitcasaClient
    {
        /// <summary>
        /// 客户端 Secret
        /// </summary>
        public string ClientSecret { get; set; }

        /// <summary>
        /// 当前用户 token
        /// </summary>
        public string Token { get; set; }

        public bool IsLogin { get { return Token != null; } }

        public BitcasaClient(string clientSecret, string token)
        {
            if (clientSecret == null)
                throw new ArgumentNullException(
                    "BitcasaSDK_BitcasaClient(): clientSecret can not be null.");

            if (token == null)
                throw new ArgumentNullException(
                    "BitcasaSDK_BitcasaClient(): token can not be null.");

            this.ClientSecret = clientSecret;
            this.Token = token;
        }

        /// <summary>
        /// 获取登录用的 Url
        /// get auth url
        /// </summary>
        public static string AuthUrl(string clientId, string callbackUrl)
        {
            if (clientId == null)
                throw new ArgumentNullException("BitcasaSDK_WP_AuthUrl(): clientId can not be null.");

            if (String.IsNullOrWhiteSpace(callbackUrl))
                throw new ArgumentException(
                    "BitcasaClient_AuthUrl(): callbackUrl can not be null or white space.");

            return String.Format("https://developer.api.bitcasa.com/v1/oauth2/authenticate?client_id={0}&redirect={1}", clientId, callbackUrl);
        }

        #region base request helper

        private void Request<T>(
            BAuthenticationRequest<T> request,
            BParameter<T> parameter)
        {
#if DEBUG
            if (!this.IsLogin)
                throw new ArgumentException(
                    "the client is logout!");

            if (request == null)
                throw new ArgumentNullException(
                    "request can not be null.");

            if (parameter == null)
                throw new ArgumentNullException(
                    "parameter can not be null.");
#endif

            if (parameter.CancelToken != null)
                parameter.CancelToken.Cancelled += (a, b) =>
                {
                    if (request == null)
                        return;

                    if (request.IsCancelled != true)
                        request.IsCancelled = true;
                };

            request.RequestSucceeded += (a, b) =>
            {
                if (parameter.Callback_Successed != null)
                    parameter.Callback_Successed(b);
            };
            request.RequestFailed += (a, b) =>
            {
                if (parameter.Callback_Failed != null)
                    parameter.Callback_Failed(b);
            };
            request.RequestCancelled += (a, b) =>
            {
                if (parameter.Callback_Cancelled != null)
                    parameter.Callback_Cancelled();
            };
            request.RequestCompleted += (a, b) =>
            {
                if (parameter.Callback_Completed != null)
                    parameter.Callback_Completed();
            };
            request.SendRequestAsync();
        }

        #endregion

        #region list item

        public void ListRoot(BParameter<List<BObject>> parameter)
        {
            ListFolder(null, parameter);
        }

        public void ListRoot(
            BitcasaSDK.BRequest.CategoryType category,
            DepthType depth,
            int count,
            BParameter<List<BObject>> parameter)
        {
            ListFolder(null, category, depth, count, parameter);
        }

        public void ListFolder(
            string path_base64,
            BParameter<List<BObject>> parameter)
        {
            var r = new BListRequest(this.Token, path_base64);
            Request<List<BObject>>(r, parameter);
        }

        public void ListFolder(
            string path_base64,
            BitcasaSDK.BRequest.CategoryType category,
            DepthType depth,
            int count,
            BParameter<List<BObject>> parameter)
        {
            var r = new BListRequest(this.Token, path_base64);
            r.Category = category;
            r.Depth = depth;
            r.Latest = count;
            Request<List<BObject>>(r, parameter);
        }

        #endregion

        #region move and copy

        public void CopyFolder(
            string path, string targetFolderPath,
            string newFolderName,
            BParameter<List<BFileSystem>> parameter,
            ExistsOperationType exists = ExistsOperationType.Rename)
        {
            var r = new BCopyOrMoveFileSystemRequest(
                false, this.Token, path, targetFolderPath, newFolderName, false);
            r.ExistsOperation = exists;
            Request<List<BFileSystem>>(r, parameter);
        }

        public void MoveFolder(
            string path, string targetFolderPath,
            string newFolderName,
            BParameter<List<BFileSystem>> parameter,
            ExistsOperationType exists = ExistsOperationType.Rename)
        {
            var r = new BCopyOrMoveFileSystemRequest(
                false, this.Token, path, targetFolderPath, newFolderName, true);
            r.ExistsOperation = exists;
            Request<List<BFileSystem>>(r, parameter);
        }

        public void CopyFile(
            string path, string targetFolderPath,
            string newFileName,
            BParameter<List<BFileSystem>> parameter,
            ExistsOperationType exists = ExistsOperationType.Rename)
        {
            var r = new BCopyOrMoveFileSystemRequest(
                true, this.Token, path, targetFolderPath, newFileName, false);
            r.ExistsOperation = exists;
            Request<List<BFileSystem>>(r, parameter);
        }

        public void MoveFile(
            string path, string targetFolderPath,
            string newFileName,
            BParameter<List<BFileSystem>> parameter,
            ExistsOperationType exists = ExistsOperationType.Rename)
        {
            var r = new BCopyOrMoveFileSystemRequest(
                true, this.Token, path, targetFolderPath, newFileName, true);
            r.ExistsOperation = exists;
            Request<List<BFileSystem>>(r, parameter);
        }

        #endregion
    }
}
