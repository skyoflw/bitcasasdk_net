﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK.BObj
{
    [DataContract]
    public abstract class BObject
    {
        [DataMember(Name = "mtime")]
        private long __mtime;
        private DateTime __modifyTime;
        public DateTime ModifyTime
        {
            get
            {
                if (__mtime > 0) // 说明是刚从网络上传输过来的
                {
                    ModifyTime = ExtMethod.ToLocalTime(__mtime);
                    __mtime = 0;
                }
                return __modifyTime.LinqTime();
            }
            set { __modifyTime = value; }
        }

        [DataMember(Name = "category")]
        private string __category_str;
        private CategoryType __category;
        public CategoryType Category
        {
            get
            {
                if (__category_str != null)
                {
                    this.Category = CategoryTypeFromString(__category_str);
                    __category_str = null;
                }
                return __category;
            }
            set { __category = value; }
        }

        [DataMember(Name = "name")]
        public string Name;

        [DataMember(Name = "mirrored")]
        public bool IsMirrored;

        /// <summary>
        /// Path base64。
        /// 对象的主键。
        /// <para/>类似于 /yvuqgtpKTy2h22thLcRtPw/cC8K2spWSCO1BFlpEAW-kA
        /// </summary>
        [DataMember(Name = "path")]
        public string Path64;

        [DataMember(Name = "type")]
        public FileSystemType Type;

        public enum FileSystemType : int
        {
            File = 0,
            Folder = 1
        }

        public bool IsFolder { get { return Type == FileSystemType.Folder; } }

        public bool IsFile { get { return Type == FileSystemType.File; } }

        private static CategoryType CategoryTypeFromString(string str)
        {
            switch (str)
            {
                case "folders":
                    return CategoryType.Folders;
                case "documents":
                    return CategoryType.Documents;
                case "photos":
                    return CategoryType.Photos;
                case "icons":
                    return CategoryType.Icons;
                case "musics":
                    return CategoryType.Music;
                case "videos":
                    return CategoryType.Videos;
                case "other":
                    return CategoryType.Other;
#if DEBUG
                default:
                    throw new NotImplementedException();
#endif
            }
            return CategoryType.Other;
        }        
    }

    public enum CategoryType : int
    {
        Folders = 0,

        Documents = 1,

        Photos = 2,

        Icons = 3,

        Music = 4,

        Videos = 5,

        Other = 6
    }
}
