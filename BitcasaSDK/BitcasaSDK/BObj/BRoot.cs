﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BitcasaSDK.BObj
{
    [DataContract]
    public class BRoot : BObject
    {
        #region member

        [DataMember(Name = "mount_point")]
        public string MountPoint;

        [DataMember(Name = "deleted")]
        public bool IsDeleted;

        [DataMember(Name = "origin_device")]
        public string OriginDevice;

        [DataMember(Name = "origin_device_id")]
        public string OriginDeviceId;

        [DataMember(Name = "sync_type")]
        private string __sync_type;
        private SyncTypeType __syncType;
        public SyncTypeType SyncType
        {
            get
            {
                if (__sync_type != null)
                {
                    SyncType = SyncTypeTypeFromString(__sync_type);
                    __sync_type = null;
                }
                return __syncType;
            }
            set { __syncType = value; }
        }

        #endregion

        private static SyncTypeType SyncTypeTypeFromString(string str)
        {
            switch (str)
            {
                case "backup":
                    return SyncTypeType.Backup;
                case "infinite drive":
                    return SyncTypeType.InfiniteDrive;
                case "regular":
                    return SyncTypeType.Regular;
                case "sync":
                    return SyncTypeType.Sync;
                case "mirrored_folder":
                    return SyncTypeType.MirroredFolder;
                case "device":
                    return SyncTypeType.Device;
                default:
                    throw new NotImplementedException("BRoot_SyncTypeTypeFromString()");
            }
        }

        public enum SyncTypeType : int
        {
            Backup = 0,
            InfiniteDrive = 1,
            Regular = 2,
            Sync = 3,
            MirroredFolder = 4,
            Device = 5
        }
    }
}
